import pandas as pd
from nltk import word_tokenize
from collections import Counter
import matplotlib.pyplot as plt
import logging
import psycopg2
import argparse
import re

attack_patterns = ["Error-based", "Union-based", "Time-based", "Boolean-based"]

attack_keywords = {
    "error-based": ["error", "syntax", "group", "order", "information_schema", "table_name",
                    "column_name", "order by", "having", "group by", "limit", "substring", "char"],
    "union-based": ["union", "concat", "limit", "union select", "union all select",
                    "concat", "from information_schema", "char", "substring", "where"],
    "boolean-based": ["not", "xor", "between", "like", "regexp", "isnull"],
    "time-based": ["sleep", "benchmark", "waitfor", "delay", "if", "else", "case"],
    "stacked-queries": [";", "--", "#", "/*"]
}


def process_dataframe(dataframe):
    del dataframe['Unnamed: 2']
    del dataframe['Unnamed: 3']
    del dataframe['Label']
    dataframe = dataframe.dropna()
    attack = []
    for index, row in dataframe.iterrows():
        if index % 1000 == 0:
            print(f"Rows of dataframe precessed: {index}")
        sentence = row["Sentence"]
        for pattern, keywords in attack_keywords.items():
            if any(keyword in word_tokenize(sentence) for keyword in keywords):
                attack.append(pattern)
                break
        else:
            attack.append(None)
    dataframe["attack_pattern"] = attack
    dataframe.to_csv("./processed_SQLiV3.csv", index=False)
    return dataframe


def analyze_queries(dataframe):
    grouped_data = dataframe.groupby("attack_pattern")["Sentence"].apply(lambda x: " ".join(x))
    word_counts = grouped_data.apply(lambda x: Counter(word_tokenize(x)))
    word_counts_df = pd.DataFrame(word_counts).reset_index(level=0).rename(
        columns={"level_0": "attack_pattern", "Sentence": "word_counts"})

    plt.figure(figsize=(30, 10))
    for _, row in word_counts_df.iterrows():
        # Get the most common words and their frequencies
        common_words = row["word_counts"].most_common(20)
        # Extract the words and frequencies as separate lists
        words, frequencies = zip(*common_words)
        plt.bar(words, frequencies, label=row["attack_pattern"])
    plt.xlabel("Keyword")
    plt.ylabel("Frequency")
    plt.legend()
    plt.show()

    # Print some statistics about the distribution of queries
    total_queries = len(dataframe)
    print(f"Total number of queries: {total_queries}")
    print("Percentage of queries by attack pattern:")
    print(dataframe["attack_pattern"].value_counts(normalize=True) * 100)
    print()
    print("Number of queries by attack pattern:")
    print(dataframe["attack_pattern"].value_counts())
    print()
    print("Minimum length of queries by attack pattern:")
    print(dataframe.groupby("attack_pattern")["Sentence"].apply(lambda x: x.str.len().min()))
    print()
    print("Maximum length of queries by attack pattern:")
    print(dataframe.groupby("attack_pattern")["Sentence"].apply(lambda x: x.str.len().max()))
    print()
    print("Mean length of queries by attack pattern:")
    print(dataframe.groupby("attack_pattern")["Sentence"].apply(lambda x: x.str.len().mean()))


def configure_logger():
    formatter = logging.Formatter(
        "\033[1m\033[32m%(levelname)s\033[0m \033[34m%(asctime)s\033[0m \033[1m\033[33m%(name)s\033[0m \033[35m%("
        "message)s\033[0m")
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)


def connect_to_database(database_name, host, port, password):
    try:
        conn = psycopg2.connect(database=database_name, host=host, port=port, password=password)
        return conn
    except psycopg2.OperationalError as e:
        logging.error(f"Failed to connect to the database: {e}")
        return


def extract_table_name(df):
    df["table_name"] = None
    table_names = []
    for index, row in df.iterrows():
        query = row["Sentence"]
        match = re.search(r"FROM (\w+)", query)
        if match:
            df.at[index, "table_name"] = match.group(1)
            table_names.append(match.group(1))
    return table_names


def create_tables(df, conn):
    cursor = conn.cursor()
    table_names = extract_table_name(df)
    for table_name in table_names:
        query = f"CREATE TABLE {table_name} (id SERIAL PRIMARY KEY);"
        logging.info(f"Executing query: {query}")
        try:
            cursor.execute(query)
            conn.commit()
            logging.info(f"Table '{table_name}' created successfully")
        except psycopg2.Error as e:
            # Log the error message and roll back the transaction
            logging.error(f"Error creating table '{table_name}': {e}")
            conn.rollback()


def execute_queries(dataframe, conn):
    cursor = conn.cursor()
    for i, row in dataframe.iterrows():
        try:
            logging.info(f"Executing {row['attack_pattern']} query {i + 1} of {len(dataframe)}: {row['Sentence']}")
            cursor.execute(row["Sentence"])
            conn.commit()
        except psycopg2.Error as e:
            logging.error(f"Failed to execute query {i + 1}: {e}")
    cursor.close()
    conn.close()


def main():
    # Create the top-level parser
    parser = argparse.ArgumentParser()

    # Add the subcommand parsers
    subparsers = parser.add_subparsers(title="subcommands", dest="subcommand")

    # Add the "analyze" subcommand parser
    analyze_parser = subparsers.add_parser("analyze")
    analyze_parser.add_argument("dataframe", type=str, help="Path to the dataframe CSV file")

    # Add the "execute" subcommand parser
    execute_parser = subparsers.add_parser("execute")
    execute_parser.add_argument("--host", type=str, required=True, help="Hostname of the database")
    execute_parser.add_argument("--port", type=int, default=5432, help="Port of the database")
    execute_parser.add_argument("--user", type=str, required=True, help="Username for the database")
    execute_parser.add_argument("--password", type=str, required=True, help="Password for the database")
    execute_parser.add_argument("--database", type=str, required=True, help="Name of the database")

    # Parse the command-line arguments
    args = parser.parse_args()

    # Configure the root logger
    configure_logger()

    df = pd.read_csv("SQLiV3.csv")
    # Check the subcommand and perform the appropriate action
    if args.subcommand == "analyze":
        # Load the dataframe
        df = process_dataframe(df)
        analyze_queries(df)
    elif args.subcommand == "execute":
        # Execute the queries
        conn = connect_to_database(args.database, args.host, args.port, args.password)
        create_tables(df, conn)
        execute_queries(df, conn)


if __name__ == "__main__":
    main()
