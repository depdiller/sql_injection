import torch.nn as nn
import pandas as pd
import torchtext.legacy as torchtext
import torch


class QueryClassificationNetwork(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size, output_size)
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        x = self.sigmoid(x)
        return x


# Load the dataframe
df = pd.read_csv("processed_SQLiV3.csv")
# Tokenize the queries
query_field = torchtext.data.Field(sequential=True, lower=True)
attack_type_field = torchtext.data.Field(sequential=False)
fields = [("Sentence", query_field), ("attack_pattern", attack_type_field)]
dataset = torchtext.data.TabularDataset("processed_SQLiV3.csv", fields=fields, format="csv")


# Build the vocabulary
query_field.build_vocab(dataset, min_freq=2)
attack_type_field.build_vocab(dataset)

# Initialize the vectors for the vocabulary to random values
query_field.vocab.vectors = torch.rand(len(query_field.vocab), 100)
attack_type_field.vocab.vectors = torch.rand(len(query_field.vocab), 100)

# Convert the data to tensors
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Split the data into train, validation, and test sets
train_dataset, val_dataset, test_dataset = torchtext.data.TabularDataset.split(
    dataset, split_ratio=[0.8, 0.1, 0.1])

# Split the dataset into train, validation, and test sets
train_data, valid_data, test_data = dataset.split(split_ratio=[0.8, 0.1, 0.1])

# Define the batch size and device
batch_size = 64
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Create the train, validation, and test iterators
train_iterator = torchtext.data.BucketIterator(
    train_data, batch_size=batch_size, device=device, repeat=False
)
valid_iterator = torchtext.data.BucketIterator(
    valid_data,  batch_size=batch_size, device=device, repeat=False
)
test_iterator = torchtext.data.BucketIterator(
    test_data,  batch_size=batch_size, device=device, repeat=False
)

# Define the neural network
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = QueryClassificationNetwork(query_field.vocab.vectors.size()[1], 64,
                                   attack_type_field.vocab.vectors.size()[1]).to(device)

# Define the optimizer and loss function
optimizer = torch.optim.Adam(model.parameters())
loss_fn = nn.CrossEntropyLoss()

# Train the neural network
for epoch in range(10):
    for batch in train_iterator:
        queries = batch.Sentence.to(device)
        labels = batch.attack_pattern.to(device)
        queries = torch.nn.functional.pad(queries, (0, 0, 0, 100 - queries.size(0)))
        queries = queries.transpose(0, 1)
        queries = queries.to(torch.float)
        output = model(queries)
        loss = loss_fn(output, labels)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    # Evaluate the neural network on the validation set
    val_loss = 0
    val_accuracy = 0
    with torch.no_grad():
        for batch in valid_iterator:
            queries = batch.Sentence.to(device)
            labels = batch.attack_pattern.to(device)
            queries = torch.nn.functional.pad(queries, (0, 0, 0, 100 - queries.size(0)))
            queries = queries.transpose(0, 1)
            queries = queries.to(torch.float)
            output = model(queries)
            loss = loss_fn(output, labels)
            val_loss += loss.item()
            val_accuracy += (output.argmax(1) == labels).sum().item()
    val_loss /= len(valid_iterator)
    val_accuracy /= len(val_dataset)

    # Print the results
    print(f"Epoch: {epoch}, Validation Loss: {val_loss:.2f}, Validation Accuracy: {val_accuracy:.2f}")

# Evaluate the neural network on the test set
test_loss = 0
test_accuracy = 0
with torch.no_grad():
    for batch in test_iterator:
        queries = batch.Sentence.to(device)
        labels = batch.attack_pattern.to(device)
        queries = torch.nn.functional.pad(queries, (0, 0, 0, 100 - queries.size(0)))
        queries = queries.transpose(0, 1)
        queries = queries.to(torch.float)
        output = model(queries)
        loss = loss_fn(output, labels)
        test_loss += loss.item()
        test_accuracy += (output.argmax(1) == labels).sum().item()
test_loss /= len(test_iterator)
test_accuracy /= len(test_dataset)

# Print the results
print(f"Test Loss: {test_loss:.2f}, Test Accuracy: {test_accuracy:.2f}")

torch.save(model.state_dict(), "./query_classification_model.pt")
