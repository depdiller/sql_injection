import torch
import torchtext
import nn_learn as nn


def main():
    # Define the fields for the dataset
    query_field = torchtext.data.Field(sequential=True, lower=True, include_lengths=True)
    attack_type_field = torchtext.data.Field(sequential=False, use_vocab=True)

    # Load the data into a dataset
    dataset = torchtext.datasets.TabularDataset(
        path="./sql_queries.csv",
        format="csv",
        fields=[("query", query_field), ("attack_type", attack_type_field)],
        skip_header=True
    )

    # Build the vocabulary for the fields
    query_field.build_vocab(dataset, min_freq=2)
    attack_type_field.build_vocab(dataset)

    # Define the neural network
    model = nn.QueryClassificationNetwork(query_field.vocab.vectors, len(attack_type_field.vocab))

    # Load the trained model weights
    model.load_state_dict(torch.load("./query_classification_model.pt"))

    # Get the query to test from user input
    query = input("Enter a SQL query to test: ")

    # Tokenize the query
    query_tokens = query_field.process([query])
    query_tensor = torch.tensor(query_tokens).unsqueeze(0)

    # Classify the query
    with torch.no_grad():
        output = model(query_tensor)
        probability = torch.nn.functional.softmax(output, dim=1)
        probability = probability.detach().numpy()[0]

    # Determine the class of the query
    attack_type = attack_type_field.vocab.itos[probability.argmax()]


    # Print the results
    print(f"Query: {query}")
    print(f"Attack Type: {attack_type}")
    print(f"Probability: {probability[probability.argmax()]:.2f}")

    if probability[probability.argmax()] >= 0.5:
        print("This query is considered malicious.")
    else:
        print("This query is considered valid.")


if __name__ == "__main__":
    main()
